<?php
require_once 'ERROR/MiError.php';

$accion = filter_input(INPUT_GET, 'action');
switch($accion){
    case 'usuarios':
        require_once '/API/UsuariosAPI.php';
        $usuariosAPI = new UsuariosAPI(); 
        $usuariosAPI->API();
        break;
    case 'login':
        require_once '/API/LoginAPI.php';
        $loginAPI = new LoginApi();
        $loginAPI->API();
        break;
    case 'idiomas':
        require_once '/API/IdiomasAPI.php';
        $idiomasAPI = new IdiomasAPI();
        $idiomasAPI->API();
        break;
    case 'preferencias':
        require_once '/API/PreferenciasAPI.php';
        $preferenciasAPI = new PreferenciasAPI();
        $preferenciasAPI->API();
        break;
    case 'salas':
        require_once '/API/SalasAPI.php';
        $salasAPI = new SalasAPI();
        $salasAPI->API();
        break;
    case 'editarUsuario':
        require_once '/API/ActualizarUsuarioAPI.php';
        $modUsuario = new EditarUsuarioAPI();
        $modUsuario->API();
        break;
    case 'editarPass':
        require_once '/API/CambiarPassAPI.php';
        $modPass = new CambiarPassAPI();
        $modPass->API();
        break;
    default:
        echo MiError::jsonError(0);
}
