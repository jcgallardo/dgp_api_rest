<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MiError
 *
 * @author jcgallardo
 */

class MiError {
    const ERRORES = array(
        array('estado' => "error", "msg" => "petición no encontrada"),  
        array('estado' => "error", "msg" => "petición no aceptada"),  
        array('estado' => "error", "msg" => "petición sin contenido"),
        array('estado' => "error", "msg" => "lista vacía de preferencias") 
    );
    public static function getError($id){
        return self::ERRORES[$id];
    }
    public static function jsonError($id){
        header('Content-Type: application/JSON');
        return json_encode(self::ERRORES[$id]);
    }
}
