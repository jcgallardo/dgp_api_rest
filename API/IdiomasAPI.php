<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IdiomasAPI
 *
 * @author jcgallardo
 */
require_once ('DB/IdiomasDB.php');
require_once ('API/Rest.php');

class IdiomasAPI extends Rest {
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {    
            case 'GET':
                $this->getIdiomas();
                break;                
            default://metodo NO soportado
                $this->mostrarRespuesta(json_encode($this->devolverError(2)), 204);
                break;
        }
    }
    
    private function devolverError($id){  
        $errores = array(  
            array('estado' => "error", "msg" => "petición no encontrada"),  
            array('estado' => "error", "msg" => "petición no aceptada"),  
            array('estado' => "error", "msg" => "petición sin contenido"),  
            array('estado' => "error", "msg" => "no hay idiomas disponibles")
        ); 
        return $errores[$id];  
    }
    
    private function getIdiomas(){
        $db = new IdiomasDB();
        $idiomas = $db->getIdiomas();
        
        if (!is_null($idiomas)){
            $response['estado'] = 'correcto';
            $response['idiomas'] = $idiomas;
            $this->mostrarRespuesta(json_encode($response),200);
        }else{
            $this->mostrarRespuesta($this->devolverError(4), 200);
        }
    }
}
