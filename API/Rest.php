<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rest
 *
 * @author jcgallardo
 */

 class Rest {  
    public $tipo = "application/json"; 
    private $_codEstado = 200;  
    
    public function __construct() {  
         
    }  
    public function mostrarRespuesta($data, $estado=200) {  
        $this->_codEstado = ($estado) ? $estado : 200;//si no se envía $estado por defecto será 200  
        $this->setCabecera();  
        echo $data;  
        exit;  
    }  
    private function setCabecera() {  
        header("HTTP/1.1 " . $this->_codEstado . " " . $this->getCodEstado());  
        header("Content-Type:" . $this->tipo . ';charset=utf-8');  
    }  
    
    private function getCodEstado() {  
        $estado = array(  
          200 => 'OK',  
          201 => 'Created',  
          202 => 'Accepted',  
          204 => 'No Content',  
          301 => 'Moved Permanently',  
          302 => 'Found',  
          303 => 'See Other',  
          304 => 'Not Modified',  
          400 => 'Bad Request',  
          401 => 'Unauthorized',  
          403 => 'Forbidden',  
          404 => 'Not Found',  
          405 => 'Method Not Allowed',  
          500 => 'Internal Server Error');  
        $respuesta = ($estado[$this->_codEstado]) ? $estado[$this->_codEstado] : $estado[500];  
        return $respuesta;  
    }  
 }  
   
