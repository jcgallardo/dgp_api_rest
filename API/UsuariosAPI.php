<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author jcgallardo
 */
require_once ('DB/UsuariosDB.php');
require_once ('API/Rest.php');

 class UsuariosAPI extends Rest{
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET'://consulta
                $this->getUsuario();
                break;     
            case 'POST'://inserta
                $this->crearUsuario();
                break;                
            default://metodo NO soportado
                $this->mostrarRespuesta($this->devolverError(2), 204);
                break;
        }
    }
    private function devolverError($id) {  
        $errores = array(  
            array('estado' => "error", "msg" => "email o password incorrectos"),  
            array('estado' => "error", "msg" => "error borrando usuario"),  
            array('estado' => "error", "msg" => "error actualizando usuario"),  
            array('estado' => "error", "msg" => "error buscando usuario por email"),  
            array('estado' => "error", "msg" => "error creando usuario"),  
            array('estado' => "error", "msg" => "usuario ya existe")  
        ); 
        return $errores[$id];  
    }
    
   private function getUsuario(){
       if($_GET['action']=='usuarios'){ 
           $db = new UsuariosDB();
           if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID
               $id = $_GET['id'];
               $response['estado'] = 'correcto';
               $response['usuario'] = $db->getUsuario($id);
               $this->mostrarRespuesta(json_encode($response), 200);  
           }else
               $this->mostrarRespuesta(MiError::jsonError(2), 204);
       }else{
           $this->mostrarRespuesta(MiError::jsonError(2), 204);
       }
   }
   
   private function crearUsuario(){
       if (filter_input(INPUT_GET, 'action') == "usuarios"){
           $db = new UsuariosDB();
           if (isset($_POST['nombre'], $_POST['correo'], $_POST['password'], $_POST['preferencia'],$_POST['idioma'])){
                $nombre = filter_input(INPUT_POST, 'nombre');
                $correo = filter_input(INPUT_POST, 'correo');
                $password = filter_input(INPUT_POST, 'password');
                $preferencia = filter_input(INPUT_POST, 'preferencia');
                $idioma = filter_input(INPUT_POST, 'idioma');

                $usuario = $db->nuevoUsuario($nombre,$correo,$password,$preferencia,$idioma);
               
                if ($usuario != null){
                    $response['estado'] = 'correcto';
                    $response['usuario'] = $usuario;
                    $this->mostrarRespuesta(json_encode($response),200);
                }else{
                    if ($db->existeUsuario($correo))
                        $this->mostrarRespuesta(json_encode($this->devolverError(5)),200);
                    else
                        $this->mostrarRespuesta(json_encode($this->devolverError(4)),200);
                }
           }else{
               $this->mostrarRespuesta(json_encode($this->devolverError(1)),200);
           }  
       }
   }
   
   
    private function modificaUsuario(){
        $correo = filter_input(INPUT_GET, "correo");
        if ($correo){
            $db = new UsuariosDB();
            $id = filter_input(INPUT_POST, "id");
            $nombre = filter_input(INPUT_POST, "nombre");
            $preferencia = filter_input(INPUT_POST, "nombre");
            $idioma = filter_input(INPUT_POST, "nombre");

            $usuario_modificado = $db->modificaUsuario($id, $correo, $nombre,$preferencia,$idioma);
            
            if ($usuario_modificado != null){
                $response['estado'] = 'correcto';
                $response['usuario'] = $usuario_modificado;
                $this->mostrarRespuesta(json_encode($response),200);
            }else{
                $this->mostrarRespuesta(json_encode($this->devolverError(2)),200);
            }
        }else{
            $this->mostrarRespuesta(MiError::jsonError(2),200);
        }
    }
   
  }
