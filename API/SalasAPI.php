<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SalasAPI
 *
 * @author jcgallardo
 */

require_once ('DB/SalasDB.php');
require_once ('API/Rest.php');

class SalasAPI extends Rest{
    private $db;
    
    public function __construct() {
        parent::__construct();
        $this->db = new SalasDB();
    }
    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET'://consulta
                if (!filter_input(INPUT_GET, "id") || filter_input(INPUT_GET, "id") == null){
                    $this->getSalas();
                }else{
                    $id = filter_input(INPUT_GET,"id");
                    $preferencia = filter_input(INPUT_GET, "preferencia");
                    $idioma = filter_input(INPUT_GET, "idioma");
                    $this->getSala($id,$preferencia,$idioma);
                }
                break;     
            case 'POST'://inserta
                $this->modificarSala();
                break;                
            default://metodo NO soportado
                $this->mostrarRespuesta($this->devolverError(2), 204);
                break;
        }
    }
    
    private function devolverError($id) {  
        $errores = array(  
            array('estado' => "error", "msg" => "no existen salas"),
            array('estado' => "error", "msg" => "no existe la sala con el id indicado")
        ); 
        return $errores[$id];  
    }
    
    private function getSalas(){
        $salas = $this->db->getSalas();
        if (!is_null($salas)){
            $response['estado'] = 'correcto';
            $response['salas'] = $salas;
            $this->mostrarRespuesta(json_encode($response),200);
        }else
            $this->mostrarRespuesta(json_encode($this->devolverError(0)),200);
    }
    
    private function getSala($id,$preferencia=null, $idioma=null){
        $sala = $this->db->getSalaConRecursos($id,$preferencia,$idioma);
        if (!is_null($sala)){
            $response['estado'] = 'correcto';
            $response['sala'] = $sala;
            $this->mostrarRespuesta(json_encode($response),200);
        }else{
            $response['estado'] = 'error';
            $this->mostrarRespuesta(json_encode($this->devolverError(1)),200);
        }
    }
    private function modificarSala($id){
        echo "Aún sin implementar.";
    }
}
