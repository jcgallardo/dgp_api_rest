<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginApi
 *
 * @author jcgallardo
 */

require_once ('DB/LoginDB.php');
require_once ('API/Rest.php');

class LoginApi extends Rest {
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {    
            case 'POST':
                $this->login($_POST['correo'],$_POST['password']);
                break;                
            default://metodo NO soportado
                $this->mostrarRespuesta($this->devolverError(2), 204);
                break;
        }
    }
    private function devolverError($id) {  
        $errores = array(  
            array('estado' => "error", "msg" => "petición no encontrada"),  
            array('estado' => "error", "msg" => "petición no aceptada"),  
            array('estado' => "error", "msg" => "petición sin contenido"),  
            array('estado' => "error", "msg" => "email o password incorrectos")
        ); 
        return $errores[$id];  
    }
    
    private function login($correo,$password){
        if (!empty($correo) and !empty($password)) {
            $db = new LoginDB();
            $usuario = $db->login($correo, $password);
            if (is_null($usuario)){
                $this->mostrarRespuesta(json_encode($this->devolverError(3)), 200);
            }else{
                $response['estado'] = 'correcto';
                $response['msg'] = 'datos del usuario';
                $response['usuario']['id'] = $usuario['id'];
                $response['usuario']['nombre'] = $usuario['nombre'];
                $response['usuario']['correo'] = $usuario['correo'];
                $response['usuario']['preferencia'] = $usuario['preferencia'];
                $response['usuario']['idioma'] = $usuario['idioma'];
                $this->mostrarRespuesta(json_encode($response),200);
            }
        }
    }
}
