<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PreferenciasAPI
 *
 * @author jcgallardo
 */
require_once ('DB/PreferenciasDB.php');
require_once ('API/Rest.php');

class PreferenciasAPI extends Rest{
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {    
            case 'GET':
                $this->getPreferencias();
                break;                
            default://metodo NO soportado
                $this->mostrarRespuesta(MiError::jsonError(2), 204);
                break;
        }
    }
    public function getPreferencias(){
        $db = new PreferenciasDB();
        $preferencias = $db->getPreferencias();
        
        if (!is_null($preferencias)){
            $response['estado'] = 'correcto';
            $response['preferencias'] = $preferencias;
            $this->mostrarRespuesta(json_encode($response),200);
        }else{
            $this->mostrarRespuesta(MiError::jsonError(4), 200);
        }
    }
}
