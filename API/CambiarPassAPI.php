<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author migadepan
 */
require_once ('DB/UsuariosDB.php');
require_once ('API/Rest.php');

class CambiarPassAPI extends Rest{
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'POST'://consulta
                $this->modificarPass();
                break;                  
            default://metodo NO soportado
                $this->mostrarRespuesta(encode(MiError::getError(0)), 204);
                break;
        }
    }
    private function devolverError($id, $mensaje = null) {  
        $errores = array(  
            array('estado' => "error", "msg" => "Error actualizando usuario: id y email no coinciden"), 
            array('estado' => "error", "msg" => "Error actualizando usuario: faltan campos"),
            array('estado' => "error", "msg" => "Error actualizando usuario: campos incorrectos"),
            array('estado' => "error", "msg" => "Error actualizando usuario: error desconocido"),
            array('estado' => "error", "msg" => "Error actualizando usuario: usuario null"),
            array('estado' => "error", "msg" => "Error actualizando usuario: ". $mensaje)
        ); 
        return $errores[$id];  
    }
    
    private function modificarPass(){
        $db = new UsuariosDB();
        $id = filter_input(INPUT_POST, 'id');
        $correo = filter_input(INPUT_POST, 'correo');
        $password = filter_input(INPUT_POST, 'password');

        if (!empty($id) && !empty($correo) && !empty($password)){
            $usuario_actualizado = $db->actualizaPass($id, $correo, $password);
            if (! is_null($usuario_actualizado)){
                $response['estado'] = 'correcto';
                $response['usuario'] = $usuario_actualizado;
                $this->mostrarRespuesta(json_encode($response), 200);
            }else{
                $this->mostrarRespuesta(json_encode($this->devolverError(4)),200);
            }
        }else{
            $this->mostrarRespuesta(json_encode($this->devolverError(1)),200);
        }
    }
}
