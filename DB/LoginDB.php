<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginDB
 *
 * @author jcgallardo
 */
require_once('DB.php');
class LoginDB extends DB{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function login($correo,$contraseña){
        $stmt = $this->mysqli->prepare("SELECT id,nombre,correo,preferencia,idioma FROM usuario WHERE  correo=? AND password=? ;");
        $stmt->bind_param("ss",$correo, $contraseña);
        $stmt->execute();
        $result = $stmt->get_result();
        
        $usuario = null;
        
        if ($result != false && $result->num_rows == 1) {
            $usuario = $result->fetch_assoc();
            $usuario_aux = $usuario;
            
            $stmt2 = $this->mysqli->prepare("SELECT tipo FROM preferencias WHERE  codigo=? ;");
            $stmt2->bind_param("s",$usuario['preferencia']);
            $stmt2->execute();
            $result2 = $stmt2->get_result();
            $tipo_aux = $result2->fetch_assoc();
            $usuario_aux['preferencia']=$tipo_aux['tipo'];
            $stmt2->close();
            
            $stmt3 = $this->mysqli->prepare("SELECT idiom FROM idioma WHERE  codigo=? ;");
            $stmt3->bind_param("s",$usuario['idioma']);
            $stmt3->execute();
            $result3 = $stmt3->get_result();
            $idiom_aux = $result3->fetch_assoc();
            $usuario_aux['idioma']=$idiom_aux['idiom'];
            $stmt3->close();
            
            $usuario = $usuario_aux;
        }
        $stmt->close();
        $this->mysqli->close();
        return $usuario;
    }
}
