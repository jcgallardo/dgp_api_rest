<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PreferenciasDB
 *
 * @author jcgallardo
 */
require_once ('DB.php');

class PreferenciasDB extends DB{
    public function __construct() {
        parent::__construct();
    }
    public function getPreferencias(){
        $preferencias = null;
        $result = $this->mysqli->query("SELECT codigo,tipo FROM preferencias ;");
        if ($result->num_rows > 0){
             $preferencias = array();
            while($row = $result->fetch_assoc()){
                array_push($preferencias,$row);
            }
        }
        $this->mysqli->close();
        return $preferencias;
    }
}
