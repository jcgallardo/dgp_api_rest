<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuariosDB
 *
 * @author jcgallardo
 */
require_once('DB.php');

class UsuariosDB extends DB{

    public function __construct() {           
        parent::__construct(); 
    } 
    /**
     * obtiene un solo registro dado su ID
     * @param int $id identificador unico de registro
     * @return Array array con los registros obtenidos de la base de datos
     */
    public function getUsuario($id=0){
        $stmt = $this->mysqli->prepare("SELECT nombre,correo,preferencias.tipo as preferencia,idioma.idiom  as idioma,fregistro,id FROM usuario, preferencias, idioma WHERE id=? and usuario.preferencia=preferencias.codigo and usuario.idioma=idioma.codigo;");
        $stmt->bind_param("s",$id);
        $stmt->execute();
        $result = $stmt->get_result();        
        $usuario = $result->fetch_assoc(); 
        $stmt->close();
        $this->mysqli->close();
            
        return $usuario;
    }
    
    public function getUsuarioPassword($id=0){
        $stmt = $this->mysqli->prepare("SELECT nombre,correo, password, preferencias.tipo as preferencia,idioma.idiom  as idioma,id FROM usuario, preferencias, idioma WHERE id=? and usuario.preferencia=preferencias.codigo and usuario.idioma=idioma.codigo;");
        $stmt->bind_param("s",$id);
        $stmt->execute();
        $result = $stmt->get_result();        
        $usuario = $result->fetch_assoc(); 
        $stmt->close();
        $this->mysqli->close();
            
        return $usuario;
    }
    public function existeUsuario($correo){
        $existe = false;
        if (filter_var($correo, FILTER_VALIDATE_EMAIL)) {  
            $stmt = $this->mysqli->prepare("SELECT correo FROM usuario WHERE correo=? ;");  
            $stmt->bind_param("s", $correo);  
            $stmt->execute();
            $result =  $stmt->get_result(); 
            $usuario = $result->fetch_assoc();
            if ($usuario != null) {  
                $existe = true;  
            }  
            $stmt->close();
        }  
        return $existe; 
    }
    
    public function nuevoUsuario($nombre,$correo,$password,$preferencia,$idioma){
            $preferenciaId = 1;
            $idiomaId = 1;
            $usuario = null;
         
            if (!$this->existeUsuario($correo)) {  
                $stmt = $this->mysqli->prepare("INSERT INTO usuario (nombre,correo,password,preferencia,idioma,fregistro) VALUES (?,?,?,?,?, NOW())");   
                
                $result2 = $this->mysqli->query("SELECT codigo FROM preferencias WHERE tipo='$preferencia'"); 
                if ($fila = $result2->fetch_assoc()){
                    $preferenciaId = $fila['codigo'];
                }
                $result3 = $this->mysqli->query("SELECT codigo FROM idioma WHERE idiom='$idioma'");
                if ($fila = $result3->fetch_assoc()){
                    $idiomaId = $fila['codigo'];
                }
                $stmt->bind_param("sssss", $nombre,$correo,$password,$preferenciaId,$idiomaId); 
                $stmt->execute();  
                
                if ($this->mysqli->affected_rows == 1){  
                    $id = $this->mysqli->insert_id;
                    $usuario = [];
                    $usuario['id'] = $id;  
                    $usuario['nombre'] = $nombre;  
                    $usuario['correo'] = $correo; 
                    $usuario['preferencia'] = $preferencia;
                    $usuario['idioma'] = $idioma;
                }
                $stmt->close();
                $this->mysqli->close();
            }  
        return $usuario;
    }

    public function actualizaUser($id, $correo, $nombre, $preferencia, $idioma) {
        $usuario_actualizado = null;
        
        // Comprobamos que el id y el correo coincidan en la misma tupla. 
        $stmt = $this->mysqli->prepare("select count(*) as total from usuario where id=? and correo=? ;");
        $stmt->bind_param("ss", $id, $correo);
        $stmt->execute();
        $result = $stmt->get_result();
        $tupla = $result->fetch_assoc();
        
        if ($tupla['total'] == 1){
            // existe el usuario con ID y correo, traemos las Id de la nueva preferencia e idioma;
            // insertamos nuevos valores
            $stmt = $this->mysqli->prepare("select preferencias.codigo as preferenciaId, idioma.codigo as idiomaId from preferencias,idioma where preferencias.tipo=? and idioma.idiom=? ;");
            $stmt->bind_param("ss", $preferencia, $idioma);
            $stmt->execute();
            $result = $stmt->get_result();
            
            if ($tupla = $result->fetch_assoc()){
                // eran datos esperados, guardamos las ID
                $preferenciaId = $tupla['preferenciaId'];
                $idiomaId = $tupla['idiomaId'];
                
                // Actualizamos usuario
                $stmt = $this->mysqli->prepare("update usuario set nombre=? , preferencia=? , idioma=? WHERE id=?");
                $stmt->bind_param("ssss", $nombre, $preferenciaId, $idiomaId, $id);
                $stmt->execute();
                
                $usuario_actualizado = $this->getUsuario($id);
            }
        }
        $stmt->close();
        $this->mysqli->close();
        return $usuario_actualizado;
    }
    
    
    public function actualizaPass($id, $correo, $pass) {
        $usuario_actualizado = null;
        
        // Comprobamos que el id y el correo coincidan en la misma tupla. 
        $stmt = $this->mysqli->prepare("select count(*) as total from usuario where id=? and correo=? ;");
        $stmt->bind_param("ss", $id, $correo);
        $stmt->execute();
        $result = $stmt->get_result();
        $tupla = $result->fetch_assoc();
        
        if ($tupla['total'] == 1){
            // Actualizamos usuario
            $stmt = $this->mysqli->prepare("update usuario set password=? WHERE id=?");
            $stmt->bind_param("ss", $pass, $id);
            $stmt->execute();

            $usuario_actualizado = $this->getUsuarioPassword($id);           
        }
        $stmt->close();
        $this->mysqli->close();
        return $usuario_actualizado;
    }

}
