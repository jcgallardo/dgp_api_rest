<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SalasDB
 *
 * @author jcgallardo
 */

require_once('DB.php');

class SalasDB extends DB{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getSalas(){
        $salas = null;
        $result = $this->mysqli->query("SELECT * from sala ;");
        if ($result->num_rows > 0){
            $salas = array();
            while ($row = $result->fetch_assoc()){
                array_push($salas, $row);
            }
        }
        $this->mysqli->close();
        return $salas;
    }
    public function getSala($id){
        $sala = null;
        $result = $this->mysqli->query("SELECT * from sala WHERE id=".$id);
        if ($result->num_rows == 1){
            $sala = $result->fetch_assoc();
        }
        $this->mysqli->close();
        return $sala;
    }
    private function transformPreferencia($preferencia){
        $tipo_multimedia = null;
        switch ($preferencia){
                case "defecto":
                    $tipo_multimedia = " IN ('audio','texto','video')";
                    break;
                case "texto":
                    $tipo_multimedia = "='texto'";
                    break;
                case "audio":
                    $tipo_multimedia = "='audio'";
                    break;
                case "video":
                    $tipo_multimedia = "='video'";
                    break;
                case "texto y audio":
                    $tipo_multimedia = " IN ('texto','audio')";
                    break;
                case "texto y video":
                    $tipo_multimedia = " IN ('texto','video')";
                    break;
                case "video y audio":
                    $tipo_multimedia = " IN ('video','audio')";
                    break;
            }
        return $tipo_multimedia;
    }
    public function getSalaConRecursos($id_sala, $preferencia=null, $idioma=null){
        $sala = null;
        $sql_sala = "SELECT * from sala WHERE id=".$id_sala;
        $sql_recurso = "SELECT * FROM recurso WHERE id_sala=".$id_sala;
        $sql_multimedia = null;
        
        $tipo_multimedia = null;
        if (is_null($preferencia) && is_null($idioma)){
            $sql_multimedia = "SELECT distinct multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion FROM multimedia,idioma,tipo_multimedia WHERE id_recurso=? AND multimedia.id_idioma=idioma.codigo AND multimedia.id_tipo=tipo_multimedia.id;";
        }else if (is_null($preferencia)){
            $sql_multimedia = "SELECT distinct multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion FROM multimedia,idioma,tipo_multimedia WHERE id_recurso=? AND multimedia.id_idioma=idioma.codigo AND multimedia.id_tipo=tipo_multimedia.id AND idioma.idiom='".$idioma."';";
        }else if (is_null($idioma)){
            $tipo_multimedia = $this->transformPreferencia($preferencia); 
            $sql_multimedia = "SELECT distinct multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion FROM multimedia,idioma,tipo_multimedia,preferencias WHERE id_recurso=? AND multimedia.id_idioma=idioma.codigo AND multimedia.id_tipo=tipo_multimedia.id AND tipo_multimedia.tipo".$tipo_multimedia.";";
        }else{
            $tipo_multimedia = $this->transformPreferencia($preferencia); 
            $sql_multimedia = "SELECT distinct multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion FROM multimedia,idioma,tipo_multimedia,preferencias WHERE id_recurso=? AND multimedia.id_idioma=idioma.codigo AND multimedia.id_tipo=tipo_multimedia.id AND idioma.idiom='".$idioma."' AND tipo_multimedia.tipo".$tipo_multimedia.";";
        }
        
        $result = $this->mysqli->query($sql_sala);
        if ($result->num_rows == 1){
            $sala = $result->fetch_assoc();
           
            //Nos traemos los recursos asociados
            $result_recursos = $this->mysqli->query($sql_recurso);
            if ($result_recursos->num_rows > 0){
                $recursos = array();
                while ($recurso = $result_recursos->fetch_assoc()){
                    $stmt = $this->mysqli->prepare($sql_multimedia);
                    $stmt->bind_param("s",$recurso['id']);
                    $stmt->execute();
                    $result_multimedia = $stmt->get_result();
                    $multimedias = array();
                    while($multimedia = $result_multimedia->fetch_assoc()){
                        array_push($multimedias, $multimedia);
                    }
                    $recurso['multimedia'] = $multimedias;
                    array_push($recursos, $recurso);
                }
                $sala['recursos'] = $recursos;
            }  
        }
        $this->mysqli->close();
        return $sala;
    }
}
