<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IdiomasDB
 *
 * @author jcgallardo
 */

require_once 'DB.php';

class IdiomasDB extends DB {
    public function __construct() {
        parent::__construct();
    }
    public function getIdiomas(){
        $idiomas = null;
        $result = $this->mysqli->query("SELECT codigo,idiom FROM idioma ;");
        if ($result->num_rows > 0){
             $idiomas = array();
            while($row = $result->fetch_assoc()){
                array_push($idiomas,$row);
            }
        }     
        return $idiomas;
    }
}
